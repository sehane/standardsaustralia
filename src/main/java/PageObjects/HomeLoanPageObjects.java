package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomeLoanPageObjects {
    WebDriver driver;

    public HomeLoanPageObjects(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//div[@class='nabrwd-banner hidden-xs']//p[text()='Request a call back']")
    public WebElement CallBackButton;

    //@FindBy(xpath = "//span[text()='Enquire now']")
    //public WebElement CallBackButton;
}

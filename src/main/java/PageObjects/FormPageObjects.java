package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormPageObjects {
    WebDriver driver;

    public FormPageObjects(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//span[text()='No']")
    public WebElement NoButton;

    @FindBy(id = "field-page-Page1-aboutYou-firstName")
    public WebElement FNameField;

    @FindBy(id = "field-page-Page1-aboutYou-lastName")
    public WebElement LNameField;

    //@FindBy(id = "field-page-Page1-aboutYou-state")
    //public WebElement StateDropDown;

    @FindBy(id = "field-page-Page1-aboutYou-phoneNumber")
    public WebElement NumberField;

    @FindBy(id = "field-page-Page1-aboutYou-email")
    public WebElement EmailField;

    @FindBy(id = "page-Page1-btnGroup-submitBtn")
    public WebElement SubmitButton;

    @FindBy(xpath = "//div[starts-with(@class, 'css-1hwfws3 react-select__value-container')]")
    public WebElement StateDropDown;


}

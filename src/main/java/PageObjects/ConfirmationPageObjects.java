package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmationPageObjects {
    WebDriver driver;

    public ConfirmationPageObjects(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//h3[contains(text(),'Thank')]")
    public WebElement ThankYouHeader;

    @FindBy(xpath = "//h1")
    public WebElement Confirmation;
}

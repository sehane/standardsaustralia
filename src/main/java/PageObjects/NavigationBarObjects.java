package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationBarObjects {
WebDriver driver;

public NavigationBarObjects(WebDriver driver){
    this.driver = driver;
    PageFactory.initElements(driver,this);
}


@FindBy(xpath = "//div[@id = 'header-container']//a[text()='Personal']")
public WebElement PersonalDropDown;

@FindBy(xpath = "//a[@class='menu-trigger'][@href = '/personal/home-loans']")
public WebElement HomeLoanLinkParent;

@FindBy(xpath = "//li[@class = 'parent']//a[@href = '/personal/home-loans']")
public WebElement HomeLoanSubLink;



    @FindBy(xpath = "//a[text()='Home loans']")
    public WebElement HomeLoanLink;

    public WebElement getHomeLoanLink() {
        return HomeLoanLink;
    }
}


package StepDefinitions;


import PageObjects.ConfirmationPageObjects;
import PageObjects.FormPageObjects;
import PageObjects.HomeLoanPageObjects;
import PageObjects.NavigationBarObjects;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class HomeLoanSteps {

    WebDriver driver;
    WebDriverWait wait20;

    @Before(value = "@driver")
    public void beforeScenario() {
        //Chrome Testing
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/Drivers/chromedriver.exe");
        driver = new ChromeDriver();


        //Firefox Testing
        //System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/Drivers/geckodriver.exe");
        //driver = new FirefoxDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        wait20 = new WebDriverWait(driver, 20);
    }

    @After(value = "@driver")
    public void afterScenario() {
        //close and quit driver instance.
        driver.close();
        driver.quit();
    }

    @Given("^The user is on the NAB website \"([^\"]*)\"$")
    public void userIsOnNabWebsite(String url) {
        //open browser to URL
        driver.get(url);
    }

    @When("The user navigates to the Home Loan page")
    public void userNavigatesToHomeLoanPage() {
        //initialise Navigation Bar page object instance
        NavigationBarObjects nbo = new NavigationBarObjects(driver);

        //navigate to personal Home Loans page
        wait20.until(ExpectedConditions.elementToBeClickable(nbo.PersonalDropDown)).click();
        wait20.until(ExpectedConditions.elementToBeClickable(nbo.HomeLoanLinkParent)).click();
        wait20.until(ExpectedConditions.elementToBeClickable(nbo.HomeLoanSubLink)).click();
    }

    @And("The user navigates to the Request a Call Back page")
    public void userNavigatesToRequestCallBackPage() {
        //initialise Home Loan page object instance
        HomeLoanPageObjects hlp = new HomeLoanPageObjects(driver);

        //click on the Request for a Call Back button
        wait20.until(ExpectedConditions.elementToBeClickable(hlp.CallBackButton)).click();

    }

    @Then("^The user is able to fill out with \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" and submit the form$")
    public void userIsAbleToFillOutCallBackForm(String fName, String lName, String Number, String Email) {
        //Due to shadow DOM, use javascript executor to find new home loans option and click next
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement newHomeLoanButton = (WebElement) jse.executeScript("return document.querySelector('div#contact-form-shadow-root').shadowRoot.querySelector('input[name=\"homeLoanRadio\"]')");
        WebElement NextButton = (WebElement) jse.executeScript("return document.querySelector('div#contact-form-shadow-root').shadowRoot.querySelector('button[data-component-id=\"Button\"]')");
        jse.executeScript("arguments[0].click()", newHomeLoanButton);
        String oldTab = driver.getWindowHandle();
        jse.executeScript("arguments[0].click()", NextButton);

        //Switch to new tab
        ArrayList<String> Tabs = new ArrayList<>(driver.getWindowHandles());
        Tabs.remove(oldTab);
        driver.switchTo().window(Tabs.get(0));
        FormPageObjects fpo = new FormPageObjects(driver);

        //Enter details into fields
        wait20.until(ExpectedConditions.visibilityOf(fpo.NoButton)).click();
        fpo.FNameField.sendKeys(fName);
        fpo.LNameField.sendKeys(lName);
        fpo.NumberField.sendKeys(Number);
        fpo.EmailField.sendKeys(Email);
        fpo.StateDropDown.click();
        Actions action = new Actions(driver);
        action.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN, Keys.ENTER)).build().perform();

        //Submit form
        //action.moveToElement(fpo.SubmitButton).click().build().perform();
        //fpo.SubmitButton.click();
        jse.executeScript("arguments[0].click()", fpo.SubmitButton);
    }

    @And("^Confirm submission using \"([^\"]*)\"$")
    public void confirmSubmission(String fName) {
        //Verify confirmation page contains first name
        ConfirmationPageObjects cpo = new ConfirmationPageObjects(driver);
        wait20.until(ExpectedConditions.visibilityOf(cpo.ThankYouHeader));
        Assert.assertTrue(cpo.ThankYouHeader.getText().toLowerCase().contains(fName.toLowerCase()));
        Assert.assertTrue(cpo.Confirmation.getText().toUpperCase().contains("WE'VE RECEIVED YOUR REQUEST"));
    }


}

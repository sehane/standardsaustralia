package StepDefinitions;
import io.cucumber.java.en.*;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.ArrayList;
public class APITest2 {
    RequestSpecification request;
    Response response;
    String temp;
    @Given("The user sets the API request URL")
    public void The_user_sets_the_api_request_URL() {
        RestAssured.baseURI = "http://api.weatherbit.io/v2.0/current";
    }
    @Given("User authenticates API request")
    public void user_authenticates_api_request() {
        request = given().queryParam("key","a1266a28a14f459ab205c29cc4b7fb8b")
                .contentType(ContentType.JSON);
    }
    @When("^I send a get request with \"([^\"]*)\"$")
    public void i_send_a_get_request_with_post_code(String postCode) {
        temp = postCode;
        request = request.queryParam("postal_code",postCode);
        response = request.when().get();

        //request = request.pathParam("postal_code",postCode);
        //response = request.when().get("/{postal_code}");
    }

    @Then("I should get a response with the timestamp_utc, weather at that point")
    public void i_should_get_a_response_with_the_timestamp_utc_weather_at_that_point() {
        String jsonString = response.asPrettyString();
        System.out.println(jsonString);
        Assert.assertEquals(response.getStatusCode(),200);
        ArrayList time = response.path("data.datetime");
        ArrayList weather = response.path("data.weather.description");
        System.out.println("For postcode " +  temp + ":");
        System.out.println("The datetime is " + time);
        System.out.println("The weather is " + weather);
    }

}

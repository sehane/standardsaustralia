package StepDefinitions;

import io.cucumber.java.en.*;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.ArrayList;

public class APITest1 {
    RequestSpecification request;
    Response response;
    String tempLat;
    String tempLon;
    @Given("^The user sets the base API request (.*)$")
    public void initKey(String url){
        RestAssured.baseURI = url;

    }

    @And("^User authenticates the API request with (.*)$")
    public void authenticateWithKey(String token){
        request = given().queryParam("key",token)
                                    .contentType(ContentType.JSON);
    }

    @When("^I send a get request with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void sendRequest(String lat, String lon){
        tempLat = lat;
        tempLon = lon;
        request = request.queryParam("lat",lat).queryParam("lon",lon);
        response = request.when().get();
    }

    @Then("I should get a response with the state code at that point")
    public void responseWithStateCode(){
        String jsonString = response.asPrettyString();
        System.out.println(jsonString);
        Assert.assertEquals(response.getStatusCode(),200);
        ArrayList stateCode = response.path("data.state_code");
        System.out.println("At point Lattitude " + tempLat + " and Longitude " + tempLon);
        System.out.println("The state code is " + stateCode);
    }


}

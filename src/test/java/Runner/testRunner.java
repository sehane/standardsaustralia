package Runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Features" ,
        glue = {"StepDefinitions"}
        , plugin = {"pretty" , "html:target/HTMLReports/report.html"}
        //, plugin = {"pretty", "junit:target/JUnitReports/report.xml"}
)
public class testRunner {
}

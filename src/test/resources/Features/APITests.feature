Feature: API Tests against Weatherbit API
  In order to get the state code
  As an authorised user
  I want to send a GET request for a specific lattitude and longitude

  Scenario Outline: State Code
    Given The user sets the base API request <url>
    And User authenticates the API request with <token>
    When I send a get request with "<lat>" and "<lon>"
    Then I should get a response with the state code at that point

    Examples:
    |url                                    |token                            |lat        |lon        |
    |https://api.weatherbit.io/v2.0/current |a1266a28a14f459ab205c29cc4b7fb8b |40.730610  |-73.935242 |

Scenario Outline: Postal Code
  Given The user sets the API request URL
  And User authenticates API request
  When I send a get request with "<Post Code>"
  Then I should get a response with the timestamp_utc, weather at that point

  Examples:
  |Post Code  |
  |28546      |
  |90210      |
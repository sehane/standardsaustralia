Feature: Enquire about a home loan

  @driver
  Scenario Outline: User enquires about a home loan by submitting a call back form
    Given The user is on the NAB website "<url>"
    When The user navigates to the Home Loan page
    And The user navigates to the Request a Call Back page
    Then The user is able to fill out with "<fname>", "<lname>", "<number>" and "<email>" and submit the form
    And Confirm submission using "<fname>"

    Examples:
    | url                     | fname     | lname     | number        | email               |
    | https://www.nab.com.au/ | Steven    | Gerrard   | 0790775435    | s.gerrad@gmail.com  |
########################################### This framework is for the Standards Australia Technical Test ########################################################

<----------------------------------------------------------------------- Tech Exercise ------------------------------------------------------------------------------------------->
Guidelines
• Use your preferred programming language. However, our preferred programming language is Java or Javascript.
• Submit your solution by creating a new repository on BitBucket with a readme file that details howto build, Run tests, etc. Share your repository link.
• Use all coding standards, design patterns, OOPS concepts, etc.
• Make test as data-driven using BDD Cucumber Framework

Ui Framework
Navigate to Nab website. Go to Home Loans. Enquire about new home loan.
Fill up the call back form and submit
API Automation
Register to get the API token key - https://www.weatherbit.io/account/create
Navigate to https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/and automate below APIs
a. GET /current?lat={lat}&lon={lon}for values {lat} as 40.730610 and {lon} as -73.935242
It should parse the response and get the value of the /data/state_code
b. GET /forecast/3hourly?postal_code={postal_code}
It should parse the response and get the value of the { timestamp_utc, weather} for all the data entries


<------------------------------------------------- How to Use Framework ------------------------------------------------------------------------------------------------------------>
Note you will have to import the maven dependencies and potentially configure your IDE. I used Java 13. Make sure the project settings and IDE settings are correct.

Driver setup is listed in the HomeLoanSteps Class file under the StepDefinitions.
Depending on your browser versions, the Driver package may need to be modified with the relevant driver.exe files.
Note sometimes the home loan page where the user can go to the call back form page dynamically changes between 2 variants.
For the first variant where there is a request for a call back link use the first web element in HomeLoansPageObjects and comment out the second.
For the second variant where there is an enquire now green button, use the second web element in HomeLoansPageObjects and comment out the first.

APITest1 and APITest2 test for both part a and b of the API Automation section. Unfortunately, the API for part b is unavailable so this could not be fully tested.
Instead the postcode version of the "current" API was used.

You can run the tests individually via feature files as well as through the testRunner class under the Runner package.
If the testRunner class is used, then an HTML report is produced in the target file. Similarly, a junit test report may be outputted by undoing the comment characters on that line.